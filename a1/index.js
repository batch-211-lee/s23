let trainer = {
	name : "Nate",
	age : 10,
	Pokemon : ["Dialga", "Palkia", "Giratina", "Zekrom", "Reshiram", "Kyurem"],
	friends : {
		Kanto : ["Lance", "Agatha", "Bruno", "Lorelei"],
		Johto : ["Koga", "Karen", "Will"],
		Hoenn : ["Drake", "Phoebe", "Glacia", "Sydney"]
	},
	talk : function(){
		console.log("Giratina! I choose you!" )
	}
};

console.log(trainer);
console.log("Results of dot notation: ");
console.log(trainer.name);
console.log("Results of square bracket notation: ");
console.log(trainer["Pokemon"]);
trainer.talk();

function Pokemon(name, level){
		this.name = name;
		this.level = level;
		this.health = level*2;
		this.attack = level*1.5;
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name);
			target.health -= this.attack
			console.log(target.name + " health is now reduced to " + target.health)
			if(target.health <= 0){
				target.faint()
		}
	}
	this.faint = function(){
		console.log(this.name + " fainted")
		}
};
let charizard = new Pokemon ("Charizard", 56);
let meganium = new Pokemon ("Meganium", 60);
let swampert = new Pokemon ("Swampert", 80);
let haxorus = new Pokemon ("Haxorus", 100);

console.log(charizard);
console.log(meganium);
console.log(swampert);
console.log(haxorus);

haxorus.tackle(charizard);
console.log(charizard);
meganium.tackle(swampert);
console.log(swampert);
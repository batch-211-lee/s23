console.log("Hello World");

//Object
/*
	a data type that is used to represent real world objects
	-it is a collection of related data and/or functionalities
	-Information stored in objects are represented in a "key:value" pair

	"key"- "property" of an object
	"value" - actual data to be stored

	different data type may be stored in an object's property creating complex data structures
*/

//There are two ways in creating objects in JS
	// 1. Object Literal Notation (let object = {}) **curly braces/object literals
	//2. Object Constructor Notation
		//Object Instantation (let object = new Object())

//Object Literal Notation
	//creating objects using initializer/literal notion
	//camelCase
	/*
		Syntax:
			let objectName = {
				keyA : valueA;
				keyB : value B;
			}
	*/

	let cellphone = {
		name : "Nokia 3210",
		manufactureDate : 1999
	}

	console.log("Results from creating objects using literal notation: ");
	console.log(cellphone);

	let cellphone2 = {
		manufactureDate : 2009,
		name : "Sony Xperia"
	}

	console.log(cellphone2);

	let ninja = {
		name : "Naruto Uzumaki",
		village: "Konoha",
		children : ["Boruto", "Himawari"]
	};

	console.log(ninja);

	//Object Constructor Notation
		//Creating Objects using a constructor function
			//Create a reusable "function" to create several objects that have the same data structure
			/*
				Syntax:
					function objectName(keyA,keyB){
						this.keyA = keyA;
						this.keyB = keyB;
					}
			*/
			//"this" keyword refers to the properties within the object
				//it allows the assignment of new object's properties
				//by associating them with values received from the constructor function's parameter
	function Laptop(name,manufactureDate){
		this.name = name;
		this.manufactureDate = manufactureDate;
	}
	//create an instance object using the Laptop constructor
	let laptop = new Laptop("Lenovo", 2008);
	console.log("Results from creating objects using object constructor");
	console.log(laptop);

	//the "new" operator creates an instance of an object (new object)
	let myLaptop = new Laptop("Macbook Air", 2012);
	console.log("Results from creating objects using object constructor");
	console.log(myLaptop);

	let myLaptop1 = new Laptop("Dell Inspiron", 2019);
	console.log(myLaptop1);

	let myLaptop2 = new Laptop("Asus", 2010);
	console.log(myLaptop2);

	let myLaptop3 = new Laptop("HP", 2014);
	console.log(myLaptop3);

	let oldLaptop = Laptop("Portal R2E CCMC", 1980)
		console.log("Results from creating objects using object constructor");
	console.log(oldLaptop);// undefined cause no "new"

	//Create Empty Objects

	let computer = {};
	let myComputer = new Object ();
	console.log(computer);
	console.log(myComputer);


//Accessing Object Properties

//Using dot notation
	console.log("Results from dot notation: " + myLaptop.name);

//Using square bracket notation
	console.log("Results from square notation: " + myLaptop["name"]);

//Accessing array of objects
//accessing object properties using the square bracket notation and array indexes can cause confusion

let arrayObj = [laptop, myLaptop];
//may be confused for accessing array indexes
console.log(arrayObj[1]["name"]);
//this tells us that array[0] is an object by using the dot notation
console.log(arrayObj[1].name);

//Initializing/Adding/Deleting/Reassigning Object Properties

//create an object using object literals
let car = {};
console.log("Current value of car object: ");
console.log(car);//{}

//Initializing/Adding object properties using dot notation
car.name = "Honda Civic";
console.log("Results from adding properties using dot notation: ");
console.log(car);//{name : "Honda Civic"}

//Initializing/Adding object properties using bracket (NOT RECOMMENDED)

console.log("Results from adding properties using square bracket notation: ");
car["Manufacture date"] = 2019;
console.log(car);

//Deleting object properties
delete car ["Manufacture date"];
console.log("Results from deleting properties: ");
console.log(car);

car["manufactureDate"] = 2019;
console.log(car);

//Reassigning object property values

car.name = "Toyota Vios";
console.log("Results from reassigning properties: ");
console.log(car);

//Object method
	//a method is a function which is a property of an object

	let person = {
		name : "John",
		talk : function(){
			console.log("Hello, my name is " + this.name)
		}
	};

	console.log(person);
	console.log("result from object methods: ");
	person.talk();

	person.walk = function(steps){
		console.log(this.name + " walked " + steps + " steps forward")
	};
	console.log(person);
	person.walk(50);

	//Methods are useful in creating reusable functions that perform tasks related to objects

	let friend = {
		firstName : "Joe",
		lastName : "Smith",
		address : {
			city: "Austin",
			country : "Texas"
		},
		emails : ["joe@mail.com", "joesmith@hmail.yz"],
		introduce : function(){
			console.log("Hello my name is " + this.firstName + " " + this.lastName + " " + "I live in " + this.address.city + ", " + this.address.country)
		}
	}

	friend.introduce();

	//Real World application of Objects
	/*
		Scenerio:
		1. We would like to create a game that would have several Pokemon interact with each other
		2. Every Pokemon would have the same set of stats, properties, and functions

		stats:

		name :
		level :
		health : level * 2
		attack : level
	*/

	//create an object constructor to lessen the process of creating the Pokemon

	function Pokemon(name, level){
		this.name = name;
		this.level = level;
		this.health = level*2;
		this.attack = level;
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name);
			target.health -= this.attack

			console.log(target.name + " health is now reduced to " + target.health)
		if(target.health <= 0){
			target.faint()
		}

	}
			this.faint = function(){
			console.log(this.name + " fainted")
		}
}

let axew = new Pokemon("Axew", 88);
let ratata = new Pokemon ("Ratata", 10);

axew.tackle(ratata);

ratata.tackle(axew);